package com.my.dubbo.bean;

import java.io.Serializable;

public class TestA implements Serializable {

	private static final long serialVersionUID = 1L;

	private String a1;

	private Integer a2;

	private boolean a3;

	public String getA1() {
		return a1;
	}

	public void setA1(String a1) {
		this.a1 = a1;
	}

	public Integer getA2() {
		return a2;
	}

	public void setA2(Integer a2) {
		this.a2 = a2;
	}

	public boolean isA3() {
		return a3;
	}

	public void setA3(boolean a3) {
		this.a3 = a3;
	}

}
