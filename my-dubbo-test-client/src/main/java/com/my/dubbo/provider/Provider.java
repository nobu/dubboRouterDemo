package com.my.dubbo.provider;

import java.util.List;

import com.my.dubbo.bean.TestA;
import com.my.dubbo.bean.TestB;

public interface Provider {

	public TestB testb(TestA testA);

	public List<TestB> testbList(TestA testA);
}
