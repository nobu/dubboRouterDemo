package com.my.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.my.dubbo.bean.TestA;
import com.my.dubbo.bean.TestB;
import com.my.dubbo.provider.Provider;

@Service
public class TestService {

	private static final Logger logger = LogManager.getLogger(TestService.class);

	@Autowired
	@Qualifier("providerService")
	private Provider providerService;

	public TestB testService() {
		logger.info("testService");
		TestA testA = new TestA();
		testA.setA1("a1");
		testA.setA2(0);
		testA.setA3(false);
		return providerService.testb(testA);
	}

	public List<TestB> testServiceList() {
		logger.info("testServiceList");
		TestA testA = new TestA();
		testA.setA1("a1 list");
		testA.setA2(0);
		testA.setA3(false);
		return providerService.testbList(testA);
	}
}
