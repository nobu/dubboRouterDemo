/*
 * Copyright 1999-2011 Alibaba Group.
 *  
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.my;


import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alibaba.dubbo.common.Constants;
import com.alibaba.dubbo.common.URL;
import com.alibaba.dubbo.rpc.Invocation;
import com.alibaba.dubbo.rpc.Invoker;
import com.alibaba.dubbo.rpc.RpcInvocation;
import com.alibaba.dubbo.rpc.cluster.Router;
import com.alibaba.dubbo.rpc.cluster.router.MockInvoker;
import com.alibaba.dubbo.rpc.cluster.router.script.ScriptRouterFactory;
import com.my.dubbo.bean.TestA;

public class MyScriptRouterTest {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }
    
    private URL SCRIPT_URL = URL.valueOf("script://javascript?type=javascript");
    
    private URL getRouteUrl(String rule) {
        return SCRIPT_URL.addParameterAndEncoded(Constants.RULE_KEY, rule);
    }
    
    
    @Test
    public void testRoute_Js(){
    	/*
        String rule = "var result = new java.util.ArrayList(invokers.size());" +
        		"for (i=0;i<invokers.size(); i++){ " +
        		    "if (20881 == invokers.get(i).getUrl().getPort()) {" +
        		        "result.add(invokers.get(i)) ;" +
        		    "}" +
        		"} ; " +
        		"return result;";
        		*/
    	
    	/*
    	String rule = "var result = new java.util.ArrayList(invokers.size());" + 
    			"	if(\"testb\".equals(invocation.getMethodName())){" + 
    			"		for(i =0;i<invocation.getArguments().length;i++){" + 
    			"			if(invocation.getArguments()[i] instanceof com.my.dubbo.bean.TestA){" + 
    			"        		var arguments = invocation.getArguments()[i];" + 
    			"        		if(\"a1\".equals(arguments.getA1())){" + 
    			"        			for (j=0;j<invokers.size(); j++){" + 
    			"        				if (20881 == invokers.get(j).getUrl().getPort()) {" + 
    			"        					result.add(invokers.get(j)) ;" + 
    			"        				}" + 
    			"        			}" + 
    			"        		}" + 
    			"        	}" + 
    			"        };" + 
    			"        return result;" + 
    			"	}else{" + 
    			"		return invokers;" + 
    			"	}";
    	*/
    	
    	String rule = "var result = new java.util.ArrayList(invokers.size());" + 
    			"for(i =0;i<invocation.getArguments().length;i++){" + 
    			"if(invocation.getArguments()[i] instanceof com.my.dubbo.bean.TestA){" + 
    			"	var arguments = invocation.getArguments()[i];" + 
    			"	if(\"a1\".equals(arguments.getA1())){" + 
    			"        for (j=0;j<invokers.size(); j++){" + 
    			"        	if (20881 == invokers.get(j).getUrl().getPort()) {" + 
    			"        		result.add(invokers.get(j)) ; return result;" + 
    			"        	}" + 
    			"        }" + 
    			"	}" + 
    			"	return invokers;" + 
    			"}"+ 
    			"}; if(result.size() == 0){return invokers;};";
        String script = "function route(invokers,invocation,context){" + rule + "} route(invokers,invocation,context)";
        Router router = new ScriptRouterFactory().getRouter(getRouteUrl(script));
    	
    	List<Invoker<String>> invokers = new ArrayList<Invoker<String>>();
        Invoker<String> invoker1 = new MockInvoker<String>(com.alibaba.dubbo.common.URL.valueOf("dubbo://10.201.8.122:20880/com.my.dubbo.provider.Provider?anyhost=true&application=my_dubbo_test_provider_1&default.accesslog=true&default.threads=10&dubbo=2.5.4-SNAPSHOT&generic=false&group=zk&interface=com.my.dubbo.provider.Provider&methods=testbList,testb&pid=9448&revision=0.0.1-SNAPSHOT&side=provider&timeout=500000&timestamp=1484732821175&version=1.0.0")) ;
        Invoker<String> invoker2 = new MockInvoker<String>(com.alibaba.dubbo.common.URL.valueOf("dubbo://10.201.8.122:20881/com.my.dubbo.provider.Provider?anyhost=true&application=my_dubbo_test_consumer&check=false&default.accesslog=true&dubbo=2.5.4-SNAPSHOT&generic=false&group=zk&interface=com.my.dubbo.provider.Provider&methods=testbList,testb&pid=6744&retries=0&revision=0.0.1-SNAPSHOT&side=consumer&timeout=500000&timestamp=1484732895484&version=1.0.0")) ;
        invokers.add(invoker1);
        invokers.add(invoker2);
        
		TestA testA = new TestA();
		testA.setA1("a2");
		testA.setA2(0);
		testA.setA3(false);
		List<TestA> listA = new ArrayList<TestA>();
		listA.add(testA);
        
        Invocation invocation = new RpcInvocation("testb",new Class[]{TestA.class}, new Object[]{listA,testA});
        
        List<Invoker<String>> fileredInvokers = router.route(invokers, invokers.get(0).getUrl(), invocation);
        Assert.assertEquals(1, fileredInvokers.size());
    }
}