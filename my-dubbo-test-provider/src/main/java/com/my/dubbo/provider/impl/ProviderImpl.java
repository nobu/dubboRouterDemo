package com.my.dubbo.provider.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.my.dubbo.bean.TestA;
import com.my.dubbo.bean.TestB;
import com.my.dubbo.provider.Provider;

@Service("providerService")
public class ProviderImpl implements Provider {

	@Override
	public TestB testb(TestA testA) {
		if (testA == null)
			return null;
		TestB testB = new TestB();
		testB.setB1("b12");
		testB.setB2(-12);
		testB.setB3(true);
		return testB;
	}

	@Override
	public List<TestB> testbList(TestA testA) {
		List<TestB> list = new ArrayList<TestB>();
		if (testA == null)
			return list;
		for (int i = 0; i < 10000; i++) {
			TestB testB = new TestB();
			testB.setB1("b" + i);
			testB.setB2(i);
			testB.setB3(true);
			list.add(testB);
		}
		return list;
	}

}
