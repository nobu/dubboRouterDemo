package com.my;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.alibaba.dubbo.common.URL;
import com.alibaba.dubbo.rpc.Invocation;
import com.alibaba.dubbo.rpc.Invoker;
import com.alibaba.dubbo.rpc.RpcInvocation;
import com.alibaba.dubbo.rpc.cluster.Router;
import com.alibaba.dubbo.rpc.cluster.router.MockInvoker;
import com.my.dubbo.bean.TestA;
import com.my.dubbo.router.AttributeRouterFactory;

public class AttributeRouterTest {

	private static final Logger logger = LogManager.getLogger(AttributeRouterTest.class);
	
	private URL getRouteUrl() {
		String rule = "attributeRouter://0.0.0.0/com.my.dubbo.provider.Provider?category=routers&dynamic=false&enabled=true&group=zk&version=1.0.0&rClassName=com.my.dubbo.bean.TestA&rAttributeName=a2&rAttributeValue=0&rIp=10.201.8.122&rPort=20881";
		URL url = URL.valueOf(rule);
		logger.info("url:{}",url.toString());
		return url;
	}

	@Test
	public void testRoute() {
		Router router = new AttributeRouterFactory().getRouter(getRouteUrl());
    	List<Invoker<String>> invokers = new ArrayList<Invoker<String>>();
        Invoker<String> invoker1 = new MockInvoker<String>(com.alibaba.dubbo.common.URL.valueOf("dubbo://10.201.8.122:20880/com.my.dubbo.provider.Provider?anyhost=true&application=my_dubbo_test_provider&default.accesslog=true&default.threads=10&dubbo=2.5.4-SNAPSHOT&generic=false&group=zk&interface=com.my.dubbo.provider.Provider&methods=testbList,testb&pid=9448&revision=0.0.1-SNAPSHOT&side=provider&timeout=500000&timestamp=1484732821175&version=1.0.0")) ;
        Invoker<String> invoker2 = new MockInvoker<String>(com.alibaba.dubbo.common.URL.valueOf("dubbo://10.201.8.122:20881/com.my.dubbo.provider.Provider?anyhost=true&application=my_dubbo_test_provider&default.accesslog=true&default.threads=10&dubbo=2.5.4-SNAPSHOT&generic=false&group=zk&interface=com.my.dubbo.provider.Provider&methods=testbList,testb&pid=9448&revision=0.0.1-SNAPSHOT&side=provider&timeout=500000&timestamp=1484732821175&version=1.0.0")) ;
        Invoker<String> invoker3 = new MockInvoker<String>(com.alibaba.dubbo.common.URL.valueOf("dubbo://10.201.8.122:20882/com.my.dubbo.provider.Provider?anyhost=true&application=my_dubbo_test_consumer&check=false&default.accesslog=true&dubbo=2.5.4-SNAPSHOT&generic=false&group=zk&interface=com.my.dubbo.provider.Provider&methods=testbList,testb&pid=6744&retries=0&revision=0.0.1-SNAPSHOT&side=consumer&timeout=500000&timestamp=1484732895484&version=1.0.0")) ;
        invokers.add(invoker1);
        invokers.add(invoker2);
        invokers.add(invoker3);
        
		TestA testA = new TestA();
		testA.setA1("a1");
		testA.setA2(0);
		testA.setA3(false);
		List<TestA> listA = new ArrayList<TestA>();
		listA.add(testA);
        
        Invocation invocation = new RpcInvocation("testb",new Class[]{TestA.class}, new Object[]{listA,testA});
        
        List<Invoker<String>> fileredInvokers = router.route(invokers, invokers.get(0).getUrl(), invocation);
        Assert.assertEquals(1, fileredInvokers.size());
	}
}
