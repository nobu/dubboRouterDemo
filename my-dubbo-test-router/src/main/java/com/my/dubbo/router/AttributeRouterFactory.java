package com.my.dubbo.router;

import com.alibaba.dubbo.common.URL;
import com.alibaba.dubbo.rpc.cluster.Router;
import com.alibaba.dubbo.rpc.cluster.RouterFactory;

public class AttributeRouterFactory implements RouterFactory {
    
    public static final String NAME = "attributeRouter";

    public Router getRouter(URL url) {
        return new AttributeRouter(url);
    }

}