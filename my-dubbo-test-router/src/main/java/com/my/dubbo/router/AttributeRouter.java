package com.my.dubbo.router;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.dubbo.common.Constants;
import com.alibaba.dubbo.common.URL;
import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.rpc.Invocation;
import com.alibaba.dubbo.rpc.Invoker;
import com.alibaba.dubbo.rpc.RpcException;
import com.alibaba.dubbo.rpc.cluster.Router;
import com.alibaba.dubbo.rpc.cluster.router.condition.ConditionRouter;

/**
 * 基础类型属性路由,不支持子类,匹配HOST+端口
 * @author zhoukai
 *
 */
public class AttributeRouter implements Router {

	private static final Logger logger = LogManager.getLogger(AttributeRouter.class);

	private static final String R_CLASS_NAME = "rClassName";
	private static final String R_ATTRIBUTE_NAME = "rAttributeName";
	private static final String R_ATTRIBUTE_VALUE = "rAttributeValue";
	private static final String R_IP = "rIp";
	private static final String R_PORT = "rPort";

	private URL url;
	private final int priority;

	private String r_ClassName;
	private String r_AttributeName;
	private String r_AttributeValue;
	private String r_Ip;
	private String r_Port;

	public AttributeRouter(URL url) {
		this.url = url;
		this.priority = url.getParameter(Constants.PRIORITY_KEY, 0);
		this.r_ClassName = url.getParameter(R_CLASS_NAME, "");
		this.r_AttributeName = url.getParameter(R_ATTRIBUTE_NAME, "");
		this.r_AttributeValue = url.getParameter(R_ATTRIBUTE_VALUE, "");
		this.r_Ip = url.getParameter(R_IP, "");
		this.r_Port = url.getParameter(R_PORT, "");
	}

	@Override
	public <T> List<Invoker<T>> route(List<Invoker<T>> invokers, URL url,
			Invocation invocation) throws RpcException {
		if (StringUtils.isEmpty(r_ClassName)
				|| StringUtils.isEmpty(r_AttributeName) || StringUtils.isEmpty(r_AttributeValue)) {
			logger.info("路由规则参数有为空,返回全部可用服务");
			return invokers;
		}

		if (StringUtils.isEmpty(r_Ip) || StringUtils.isEmpty(r_Port)) {
			logger.info("路由匹配参数有为空,返回全部可用服务");
			return invokers;
		}
		
		List<Invoker<T>> result = new ArrayList<Invoker<T>>(invokers.size());
		try {
			for (int i = 0; i < invocation.getArguments().length; i++) {
				Object rObject = invocation.getArguments()[i];
				Class<?> rclsss = rObject.getClass();
				if (r_ClassName.equals(rclsss.getName())) {
					Field[] fields = rclsss.getDeclaredFields();
					for (Field field : fields) {
						if (r_AttributeName.equals(field.getName())) {
							field.setAccessible(true);
							String value = field.get(rObject).toString();
							if (!StringUtils.isEmpty(value) && r_AttributeValue.equals(value)) {
								for (Invoker<T> invoker : invokers){  
									URL invokerUrl = invoker.getUrl();
									if(r_Ip.equals(invokerUrl.getHost()) && r_Port.equals(""+invokerUrl.getPort())){
										result.add(invoker);
										return result;
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("路由反射取值错误", e);
			e.printStackTrace();
		}
		
		logger.info("路由匹配为空返回全部可用服务");
		return invokers;
	}

	@Override
	public int compareTo(Router o) {
		if (o == null || o.getClass() != ConditionRouter.class) {
			return 1;
		}
		AttributeRouter c = (AttributeRouter) o;
		return this.priority == c.priority ? url.toFullString().compareTo(
				c.url.toFullString()) : (this.priority > c.priority ? 1 : -1);
	}

	@Override
	public URL getUrl() {
		return url;
	}

}
