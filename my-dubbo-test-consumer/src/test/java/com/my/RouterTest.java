package com.my;

import com.alibaba.dubbo.common.URL;
import com.alibaba.dubbo.common.extension.ExtensionLoader;
import com.alibaba.dubbo.registry.Registry;
import com.alibaba.dubbo.registry.RegistryFactory;

public class RouterTest {

	public static void main(String[] args) {
		// URL myUrl =
		// URL.valueOf("dubbo://10.201.8.122:20880/com.my.dubbo.provider.Provider?anyhost=true&application=my_dubbo_test_consumer&check=false&default.accesslog=true&dubbo=2.5.4-SNAPSHOT&generic=false&group=zk&interface=com.my.dubbo.provider.Provider&methods=testbList,testb&pid=9632&retries=0&revision=0.0.1-SNAPSHOT&side=consumer&timeout=500000&timestamp=1484638803708&version=1.0.0");

		// System.out.println(myUrl.getParameter("application"));
		register();
	}

	public static String getJs() {
    	String rule = "var result = new java.util.ArrayList(invokers.size());" + 
    			"for(i =0;i<invocation.getArguments().length;i++){" + 
    			"if(invocation.getArguments()[i] instanceof com.my.dubbo.bean.TestA){" + 
    			"	var arguments = invocation.getArguments()[i];" + 
    			"	if(\"a1\".equals(arguments.getA1())){" + 
    			"        for (j=0;j<invokers.size(); j++){" + 
    			"        	if (20881 == invokers.get(j).getUrl().getPort()) {" + 
    			"        		result.add(invokers.get(j)) ; return result;" + 
    			"        	}" + 
    			"        }" + 
    			"	}" + 
    			"	return invokers;" + 
    			"}"+ 
    			"}; if(result.size() == 0){return invokers;};";
        String script = "function route(invokers,invocation,context){" + rule + "} route(invokers,invocation,context);";
        return script;
	}

	public static URL getUrl() {
		// URL myUrl =
		// URL.valueOf("condition://0.0.0.0/com.my.dubbo.provider.Provider?category=routers&dynamic=false&enabled=true&force=false&group=zk&name=test1&priority=0&router=condition&version=1.0.1&rule="
		// + URL.encode("method = testb => provider.protocol = dubbo"));

		// URL myUrl =
		// URL.valueOf("script://0.0.0.0/com.my.dubbo.provider.Provider?category=routers&dynamic=false&enabled=true&force=false&group=zk&name=testJS&version=1.0.0&rule="
		// +
		// URL.encode("function route(invokers){var result=new java.util.ArrayList(invokers.size());for(i=0;i<invokers.size();i++){if('my_dubbo_test_provider_2'.equals(invokers.get(i).getUrl().getParameter('application'))){result.add(invokers.get(i))}}return result}(invokers);"));

		
//		script://javascript?rule=function+route%28invokers%2Cinvocation%2Ccontext%29%7Bvar+result+%3D+new+java.util.ArrayList%28invokers.size%28%29%29%3Bfor+%28i%3D0%3Bi%3Cinvokers.size%28%29%3B+i%2B%2B%29%7B+if+%2820881+%3D%3D+invokers.get%28i%29.getUrl%28%29.getPort%28%29%29+%7Bresult.add%28invokers.get%28i%29%29+%3B%7D%7D+%3B+return+result%3B%7D+route%28invokers%2Cinvocation%2Ccontext%29&type=javascript
		
		URL myUrl = URL
				.valueOf("script://0.0.0.0/com.my.dubbo.provider.Provider?category=routers&dynamic=false&enabled=true&group=zk&version=1.0.0&type=javascript&rule="
						+ URL.encode(getJs()));
		System.out.println(myUrl);
		return myUrl;
	}

	public static Registry getRegistry() {
		RegistryFactory registryFactory = ExtensionLoader.getExtensionLoader(
				RegistryFactory.class).getAdaptiveExtension();
		Registry registry = registryFactory.getRegistry(URL
				.valueOf("zookeeper://127.0.0.1:2181"));
		return registry;
	}

	public static void register() {
		getRegistry().register(getUrl());
	}

	public static void unregister() {
		getRegistry().unregister(getUrl());
	}

}
