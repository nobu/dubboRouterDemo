package com.my.dubbo.bean;

import java.io.Serializable;

public class TestB implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String b1;

	private Integer b2;

	private boolean b3;

	public String getB1() {
		return b1;
	}

	public void setB1(String b1) {
		this.b1 = b1;
	}

	public Integer getB2() {
		return b2;
	}

	public void setB2(Integer b2) {
		this.b2 = b2;
	}

	public boolean isB3() {
		return b3;
	}

	public void setB3(boolean b3) {
		this.b3 = b3;
	}

}
