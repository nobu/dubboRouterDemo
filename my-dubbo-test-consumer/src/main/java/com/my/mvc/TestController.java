package com.my.mvc;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.my.service.TestService;

@Controller
public class TestController {
	private static final Logger logger = LogManager.getLogger(TestController.class);

	@Autowired
	private TestService testService;

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	@ResponseBody
	public String testa(HttpServletRequest request) {
		String result = JSON.toJSONString(testService.testService());
		logger.info("test:{}", result);
		return result;
	}

	@RequestMapping(value = "/testList", method = RequestMethod.GET)
	@ResponseBody
	public String testlist(HttpServletRequest request) {
		String result = JSON.toJSONString(testService.testServiceList());
		logger.info("testList:{}", result);
		return result;
	}
}
